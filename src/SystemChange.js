import React, { Component } from 'react';
const baseUrl = '/*http://localhost:8280*/';/*http://localhost:8280*/
const dev='dev12321';
const mockData=JSON.parse(
    '{"testInfoList":[{"orgid":"1","systemType":"D系统","dataSource":"本"},{"orgid":"2","systemType":"C系统","dataSource":"库"},{"orgid":"3","systemType":"D系统","dataSource":"库"},{"orgid":"4","systemType":"C系统","dataSource":"库"}]}'
    );
class SystemChange extends Component{

    constructor(props){
        super(props);
        this.state={
            testInfoList:[],
            isFetching:true,
        }
        this.onChangeSystem= this.onChangeSystem.bind(this);
        this.changeSystem= this.changeSystem.bind(this);
    }
    
    componentDidMount(){
        this.onChangeSystem(this.state.testInfoList);
    }

    changeSystem(systemType,index){
        this.state.testInfoList[index].systemType=systemType;
        this.setState({
            testInfoList:this.state.testInfoList
         })
        }
    onChangeSystem(
        ibeInfoList=this.state.testInfoList
    ){
        this.setState({
            ifFetching:true
        })
        if(Object.keys(ibeInfoList).length===0||ibeInfoList===undefined){
            return GET(baseUrl+'/querySystemType',mockData).then((result)=>{
                this.setState({
                    testInfoList:result.testInfoList,
                    isFetching:false
                })
            })
        }else{
            return POST(baseUrl+'/changeSystemType',{ibeInfoList}).then((result)=>{
                this.setState({
                    testInfoList:result.testInfoList,
                    isFecthing:false
                })
            })
        }
        }

    render(){
        return(
            <div style={{ position: 'relative' }}>
                <div>
                    <table>
                        <tbody>
                        <tr>
                            <th>资源</th>
                            <th>类型</th>
                            <th>当前的系统</th>
                        </tr>
                        {this.state.testInfoList.map((info,index)=>{
                            return(
                                <tr>
                                    <th>{info.dataSource}</th>
                                    <th>{info.orgid}</th>
                                    <label>
                                        <input defaultChecked={info.systemType==="D系统"} type="radio" name={index}
                                               onClick={()=>this.changeSystem("D系统",index)}/>D
                                    </label>
                                    <label>
                                        <input defaultChecked={info.systemType==="C系统"} type="radio" name={index}
                                               onClick={()=>this.changeSystem("C系统",index)}/>C
                                    </label>
                                </tr>
                            )
                        })}
                        </tbody>
                    </table>
                    <div>
                    <button onClick={()=>this.onChangeSystem(this.state.testInfoLIst)}></button>
                    </div>
                </div>
            </div>
        )
    }

}

function GET(uri,mockData){
    function http(){
        return fetch(uri,{
            method:'get',
            credentials:'include',
        })
    }
    if(mockData!==undefined && baseUrl!==undefined){
        return new Promise((resolve)=>{
            setTimeout(function(){
                resolve(mockData);
            },100);
        })
    }else{
        return httpResolve(http);
    }

}

function POST(uri,testInfoList){
    function http(){
        return fetch(uri,{
            method:'post',
            headers:{
                'Content-Type':'application/json',
            },
            credentials:'include',
            body:JSON.stringify(testInfoList),
        })
    }
    return httpResolve(http);
}

function httpResolve(http){
    return http()
    .then((response)=>{
        const responseOk=response.ok;
        const status=response.status;
        if(responseOk){
            return response.json();
        }else{
            return{
                errMsg:'服务器异常'+status,
            }
        }
    })
    .catch((response)=>{
        console.error(response.message);
        return{}
    })
}

export default SystemChange;
