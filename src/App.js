import React, { Component } from 'react';
import SysteamChange from './SystemChange';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
        </header>
        <SysteamChange></SysteamChange>
      </div>
    );
  }
}

export default App;
